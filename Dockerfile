# FROM busybox:1.31.1 AS download

# ARG CHROME_DRIVER=87.0.4280.20

# WORKDIR /tmp

# RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
# RUN wget https://chromedriver.storage.googleapis.com/${CHROME_DRIVER}/chromedriver_linux64.zip && \
#     unzip chromedriver_linux64.zip

FROM python:3.8.2-slim-buster
LABEL maintainer="tle@tlnk.fr"

# ENV DISPLAY=:99

RUN apt-get update && apt-get install -y gcc \
    cron \
    python-dev \
    libxml2-dev \
    libxslt1-dev \
    zlib1g-dev \
    libffi-dev \
    libssl-dev \
    libmariadb-dev-compat \
    libmariadb-dev

COPY ./cron/. /etc/cron.d/
# COPY --from=download /tmp/google-chrome-stable_current_amd64.deb /tmp/google-chrome-stable_current_amd64.deb
# COPY --from=download /tmp/chromedriver /usr/bin/chromedriver

RUN chmod 0744 /etc/cron.d/* && \
    touch /var/log/cron.log

# RUN dpkg -i /tmp/google-chrome-stable_current_amd64.deb; apt-get -fy install && \
#     rm /tmp/google-chrome-stable_current_amd64.deb

COPY ./properties/. /app
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip3 install -r requirements.txt && \
    apt-get purge -y --auto-remove \
    gcc \
    python-dev \
    libxml2-dev \
    libxslt1-dev \
    zlib1g-dev \
    libffi-dev \
    libssl-dev \
    libmariadb-dev-compat \
    libmariadb-dev

RUN crontab /etc/cron.d/cron-scrapy

CMD ["cron", "-f"]

LABEL org.label-schema.name="properties_scraping"
LABEL org.label-schema.description="scrapy crawler"
LABEL org.label-schema.url="https://github.com/tle06/properties_scraping"
LABEL org.label-schema.vendor="TLNK"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vcs-url="https://github.com/tle06/properties_scraping"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.docker.cmd="docker run -t -i tlnk/properties_scraping"