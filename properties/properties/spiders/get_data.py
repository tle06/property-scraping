

import logging
import time
import scrapy
from ..functions import get_offers, get_root_url
from ..parsers import ParserLBC, IParser, ParserSLG
from ..slack import slack_send_message
from scrapy.loader import ItemLoader
from scrapy.utils.project import get_project_settings


logger = logging.getLogger('OFFER_DATA')


class GetDataSpider(scrapy.Spider):

    name:str = 'data'

    allowed_domains: list = get_project_settings().get('ALLOWED_DOMAINS')
    start_urls: list = ['https://www.google.com/']
    headers: dict = get_project_settings().get('DEFAULT_REQUEST_HEADERS')
    error_cost:int = 0
    error_surface = 0
    error_global:int = 0
    total_parsing:int = 0
    slack_message:str = ""

    def __init__(self):
        pass


    def start_requests(self):
        logger.info('GET OFFERS FROM DB')
        self.offers = get_offers()
        
        for o in self.offers:
            logger.info('PROCESS URL')
            logger.info('URL:%s', o.url)
            logger.info('Offer ID:%s', o.id)
            time.sleep(5)
            yield scrapy.Request(url=o.url, callback=self.parse, cb_kwargs=dict(offer=o),headers=self.headers)
        
        self.slack_message = "SCRAPER INFO:\n\n💾Total item added = " + str(self.total_parsing) + "\n❌Total item with cost error = " + str(self.error_cost) + "\n❌Total item with surface error = " +str(self.error_surface)+ "\n❌Total item with cost and surface error = " +str(self.error_global)+ "\n✔️Sucess ratio = " + str((self.total_parsing-self.error_surface-self.error_cost+self.error_global)/self.total_parsing)

        res = slack_send_message(message=self.slack_message)
        logger.info('SLACK response: %s', res)

    def parse(self, response, offer):

        parser: IParser = None
        root_url:str = get_root_url(offer.url)
        logger.info('ROOT URL: %s', root_url)

        if offer.source_id == 2:
            parser = ParserLBC(response=response,root_url=root_url)

        if offer.source_id == 1:
            parser = ParserSLG(response=response,root_url=root_url)

        if parser:
            if parser.check_captcha():
                logger.info('CAPTCHA trigger:%s', parser.check_captcha())
                time.sleep(60)
                yield scrapy.Request(url=response.url, callback=self.parse, cb_kwargs=dict(offer=offer),headers=self.headers)
            else:

                # Data from current offer
                id = offer.id
                city = offer.city
                country = offer.country
                type_id = offer.type_id
                source_id = offer.source_id
                url = offer.url
                logger.info('OFFER ID :%s', id)

                # Data parse by parser object

                if offer. surface > 0:
                    surface = offer.surface
                    logger.info('EXISTING SURFACE: %s', surface)
                else:
                    surface = parser.parse_surface()
                    logger.info('SURFACE found: %s', surface)
                room = parser.parse_room()
                logger.info('ROOM found: %s', surface)
                description = parser.parse_description()

                
                if offer.cost >0 :
                    cost = offer.cost
                    logger.info('EXISTNG COST: %s', cost)
                else:
                    cost = parser.parse_cost()
                    logger.info('COST found: %s', cost)
                
                cost_per_meter = parser.parse_cost_per_meter()
                logger.info('COST/SURFACE found: %s', cost_per_meter)
                disabled:bool = parser.get_disabled()
                logger.info('OFFER is disabled: %s', disabled)

                if cost==0 and surface ==0:
                    self.error_global = self.error_global + 1
                if cost == 0:
                    self.error_cost = self.error_cost + 1
                
                if surface == 0:
                    self.error_surface = self.error_surface + 1

                item:ItemLoader = None
                item = parser.generate_item(type_id=type_id,source_id=source_id,url=url,city=city, country=country,surface=surface,room=room,description=description,cost=cost,cost_per_meter=cost_per_meter, disabled=disabled)
                logger.info('ITEM Generated:%s', item)
                logger.info('LOAD ITEM')
                self.total_parsing = self.total_parsing +1
                yield item.load_item()


        else:
            logger.info('NO PARSER CREATED')
        
        logger.info('Total item parsed: %s', self.total_parsing)
        logger.info('Total cost error: %s', self.error_cost)
        logger.info('Total surface error: %s', self.error_surface)
        logger.info('Total global error: %s', self.error_global)
        logger.info('Success ratio: %s', (self.total_parsing-self.error_surface-self.error_cost+self.error_global)/self.total_parsing)
