import re
from sqlalchemy.orm import sessionmaker
from .model import db_connect
import logging

logger = logging.getLogger('FUNCTION')

def right(s, amount):
    return s[-amount:]

def search_array(array, search):
    return [s for s in array if any(xs in s for xs in search)]

def search_number_array(array:list):
    logger.info('ARRAY INIT: %s', array)
    array = [a.replace(' ', '') for a in array]
    logger.info('ARRAY WITHOUT SPACE: %s', array)
    array = [a.replace(u'\u202f', '') for a in array]
    logger.info('ARRAY WITHOUT SPECIAL SPACE: %s', array)
    array = [re.search(r"(\d+(?:\,\d+)?)", a) for a in array]
    logger.info('ARRAY WITH NUMBERS: %s', array)
    array = [a.group() for a in array if a]
    logger.info('ARRAY GROUP: %s', array)

    if array:
        array = [a.replace(',', '.') for a in array]
        logger.info('ARRAY REPLACE, BY .: %s', array)
        if len(array) == 1:
            logger.info('ONE VALUE IN ARRAY: %s', array)
            return convert_value_to_num(array_to_str(array))
        else:
            array.sort()
            logger.info('ARRAY SORTED: %s', array)
            return convert_value_to_num(array[-1])
    else:
        logger.info('ARRAY IS EMPTY: %s', array)
        return 0

def search_number_array_by_query(array, search, exclude=''):
    if search:
        matching = search_array(array, search)
    else:
        matching = array

    if matching:
        if exclude:
            matching = exclude_from_list(matching, exclude)
        return search_number_array(matching)
    else:
        return 0

def array_to_str(array):
    return (''.join([str(elem) for elem in array])).strip()

def exclude_from_list(array, exclude):
    for a in array:
        for e in exclude:
            if e in a:
                array.remove(a)
    return array

def db_init_session():
    engine = db_connect()
    session = sessionmaker(bind=engine)()
    return session

def get_offers():
    session = db_init_session()
    offers = session.execute(f"SELECT * FROM `scrape_properties` WHERE (cost = 0 or surface = 0) and (disabled = 0 or disabled IS NULL) ORDER BY ID DESC;")
    session.close()
    return offers

def convert_value_to_num(value):
    try:
        value = float(value)
        if (value).is_integer():
            return int(value)
        else:
            return value
    except ValueError:
        return 0

def get_urls(url:str):
    session = db_init_session()
    urls = session.execute(f"Select url from scrape_offers where scrape_offers.url like '{url}%'")
    session.close()
    return urls

def create_logger(ref:str):
    logger = logging.getLogger(ref)
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger

def get_root_url(url):
    rl:str = None
    regex:str = "https?:\/\/(?:.*\.)*(.+\..+?)\/"
    rl = re.search(regex, url).group()
    rl = rl[:-1]
    return rl