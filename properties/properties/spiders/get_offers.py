import logging
import time
import scrapy
from properties.parsers import ParserLBC, IParser, ParserSLG
from properties.functions import get_root_url
from scrapy.utils.project import get_project_settings
from properties.slack import slack_send_message
from scrapy.loader import ItemLoader

logger = logging.getLogger('OFFER')

class GetOffersSpider(scrapy.Spider):

    name:str = 'offer'

    allowed_domains:list = get_project_settings().get('ALLOWED_DOMAINS')
    lbc_domain_list:list = get_project_settings().get('LBC_DOMAIN_LIST')
    slg_domain_list:list = get_project_settings().get('SLG_DOMAIN_LIST')
    headers: dict = get_project_settings().get('DEFAULT_REQUEST_HEADERS')

    start_urls:list = ['https://www.google.com/']

    #urls = [{"url":"https://www.leboncoin.fr/recherche?category=9&locations=Rennes__48.10980729840584_-1.6675040381352901_10000&page=1","city":"Rennes","source_id":2,"type_id":1,"country":"france"}]
    urls = [{"url":"https://www.leboncoin.fr/recherche?category=9&locations=Tours__47.38958347953365_0.6976761968374228_10000&real_estate_type=1%2C2&price=min-250000","city":"Tours","source_id":2,"type_id":1,"country":"france"}]
    #urls = [{"url":"https://www.leboncoin.fr/recherche?category=9&locations=Laval_53000__48.07268_-0.77307_5652_5000","city":"Laval","source_id":2,"type_id":1,"country":"france"}]
    #urls = [{"url":"https://www.leboncoin.fr/recherche?category=9&locations=Ern%C3%A9e_53500__48.29671_-0.93452_5942","city":"Ernee","source_id":2,"type_id":1,"country":"france"}]
    #urls = [{"url":"https://www.leboncoin.fr/recherche?category=10&locations=Tours__47.38958347953365_0.6976761968374228_10000&real_estate_type=1%2C2","city":"Tours","source_id":2,"type_id":2,"country":"france"}]
    #urls = [{"url":"https://www.seloger.com/list.htm?tri=initial&enterprise=0&idtypebien=1&idtt=2&naturebien=1&ci=350238&m=search_hp_new","city":"Rennes","source_id":1,"type_id":1,"country":"france"}]
    
    import_offers:list = []
    pages_list:list = []
    skip_offers:list = []
    slack_message:str = ""

    def __init__(self):
        pass
        
    def start_requests(self):
        for o in self.urls:
            logger.info('OBJECT: %s',o)
            logger.info('PROCESS URL')
            logger.info('URL:%s', o['url'])
            logger.info('City:%s', o['city'])
            logger.info('Country:%s', o['country'])
            logger.info('Srouce ID:%s', o['source_id'])
            logger.info('Type ID:%s', o['type_id'])
            time.sleep(5)
            yield scrapy.Request(url=o['url'], callback=self.parse, cb_kwargs=dict(offer=o),headers=self.headers)
    
        total_scrape_offer:str = str(len(self.skip_offers)+len(self.import_offers))
        total_page:str = str(len(self.pages_list))
        total_skip_offer: str = str(len(self.skip_offers))
        total_import_offer:str = str(len(self.import_offers))
        self.slack_message = "SCRAPER INFO:\n\nTotal offers scraped = " + total_scrape_offer + "\nTotal pages scraped = "+ total_page +"\n❌Total skip offers = " + total_skip_offer + "\n✔️Total url imported = " + total_import_offer

        res = slack_send_message(message=self.slack_message)
        logger.info('SLACK response: %s', res)

    def parse(self, response, offer):

        logger.info('OBJECT: %s',offer)
        parser: IParser = None
        root_url = get_root_url(response.url)

        if root_url in self.lbc_domain_list:
            parser = ParserLBC(response=response,root_url=root_url)
        
        if root_url in self.slg_domain_list:
            parser = ParserSLG(response=response,root_url=root_url)

        if parser:
            if parser.check_captcha():
                logger.info('CAPTCHA trigger:%s', parser.check_captcha())
                time.sleep(60)
                yield scrapy.Request(url=response.url, callback=self.parse, cb_kwargs=dict(offer=offer),headers=self.headers)
            else:

                logger.info('PASER Offers:%s', parser.get_offers())

                for o in parser.get_offers():
                    url = o
                    logger.info('OFFER URL:%s', url)
                    
                    if not parser.check_url_exist(url):
                        logger.info('URL do NOT EXIST in DATABASE')
                        self.import_offers.append(url)
                        item:ItemLoader = None
                        item = parser.generate_item(type_id=offer['type_id'],source_id=offer['source_id'],url=url,city=offer['city'], country=offer['country'])
                        logger.info('ITEM Generated:%s', item)
                        
                        yield item.load_item()
                    else:
                        logger.info('URL EXIT in DATABASE:%s')
                        self.skip_offers.append(url)

                logger.info('PASER NEXT PAGE:%s', parser.get_next_page())
                if parser.get_next_page():
                    self.pages_list.append(parser.get_next_page())
                    
                    logger.info('imported offers:%s', len(self.import_offers))
                    logger.info('Skip offers:%s', len(self.skip_offers))
                    logger.info('Total offers:%s', len(self.skip_offers)+len(self.import_offers))
                    logger.info('Count pages:%s', len(self.pages_list))
                    time.sleep(5)
                    yield scrapy.Request(url=parser.get_next_page(), callback=self.parse,cb_kwargs=dict(offer=offer),headers=self.headers)
                else:
                    logger.info('imported offers:%s', len(self.import_offers))
                    logger.info('Skip offers:%s', len(self.skip_offers))
                    logger.info('Total offers:%s', len(self.skip_offers)+len(self.import_offers))
                    logger.info('Count pages:%s', len(self.pages_list))
                    logger.info('no next page url')
        else:
            logger.info('NO PARSER CREATED')