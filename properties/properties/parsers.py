from abc import ABCMeta, abstractmethod
from scrapy.http import Response
from scrapy.loader import ItemLoader
from .items import ScrapePropertyItem
from .model import ScrapeProperties
from .functions import db_init_session,array_to_str, exclude_from_list, search_number_array, search_number_array_by_query
import logging
from datetime import datetime
import re

logger = logging.getLogger('PARSER')

class IParser(object, metaclass=ABCMeta):

    @abstractmethod
    def extract_xpath(self):
        pass

    @abstractmethod
    def extract_multi_xpath(self):
        pass

    @abstractmethod
    def check_captcha(self) -> bool:
        pass

    @abstractmethod
    def get_next_page(self) -> str: 
        pass

    @abstractmethod
    def get_offers(self) -> list:
        pass
    
    @abstractmethod
    def parse_surface(self) -> float:
        pass

    @abstractmethod
    def parse_announce_reference(self) -> str:
        pass

    @abstractmethod
    def parse_room(self) -> int:
        pass

    @abstractmethod
    def parse_description(self) -> str:
        pass

    @abstractmethod
    def parse_cost(self) -> float:
        pass

    @abstractmethod
    def parse_cost_per_meter(self) -> float:
        pass

    @abstractmethod
    def get_disabled(self) -> bool:
        pass


    def generate_item(self,type_id:int,source_id:int,url:str,city:str,country:str,surface:float=0.0,cost_per_meter:float=0.0,cost:float=0.0,address:str="",room:int=0,bedroom:int=0,bathroom:int=0,toilette:int=0,energy_indice_number:int=0,greenhouse_gas_number:int=0,construction_year:int=1900,agency_name:str="",announce_reference:str="",description="",disabled:bool=False):
        item: ItemLoader = None
        item = ItemLoader(item=ScrapePropertyItem())
        item.add_value('type_id',type_id)
        item.add_value('source_id',source_id)
        item.add_value('created_date',datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        item.add_value('url',url)
        item.add_value('surface',surface)
        item.add_value('cost_per_meter',cost_per_meter)
        item.add_value('cost',cost)
        item.add_value('address',address)
        item.add_value('city',city)
        item.add_value('country',country)
        item.add_value('room',room)
        item.add_value('bedroom',bedroom)
        item.add_value('bathroom',bathroom)
        item.add_value('toilette',toilette)
        item.add_value('energy_indice_number',energy_indice_number)
        item.add_value('greenhouse_gas_number',greenhouse_gas_number)
        item.add_value('construction_year',construction_year)
        item.add_value('agency_name',agency_name)
        item.add_value('announce_reference',announce_reference)
        item.add_value('description',description)
        item.add_value('disabled',disabled)

        return item
    
    def get_scrape_properties_by_url(self,url:str):
        data: ScrapeProperties = None
        session = db_init_session()
        data = session.query(ScrapeProperties).filter_by(url=url).first()
        session.close()
        return data
    
    def check_url_exist(self,url:str):
        check:ScrapeProperties = None
        check = self.get_scrape_properties_by_url(url)
        if check:
            return True
        else:
            return False

class ParserLBC(IParser):
    response: Response = None
    root_url: str = None

    xpath_next_url:str = "//a[@title='Page suivante']/@href"
    xpath_offers:str = '//a[@data-qa-id="aditem_container"]/@href'
    xpath_captcha:str = "/html/body/iframe/@src"
    xpath_surface:str = '//*[@data-qa-id="criteria_item_square"]//text()'
    xpath_room: str = '//*[@data-qa-id="criteria_item_rooms"]//text()'
    xpath_ref: str = '//*[@data-qa-id="criteria_item_custom_ref"]//text()'
    xpath_description: str = '//*[@data-qa-id="adview_description_container"]//text()'
    xpath_cost: str = '//*[@id="grid"]/article/section/div[3]/div[2]/div[2]/span/text()'
    xpath_disabled: str = '//*[@id="container"]/main/div/div[1]/div[3]/section/div[1]/text()'

    dict_bathroom: list = ['Salle de bain', 'Salles de bain', 'Salle d\'eau']
    dict_toilette: list = ['Toilette', 'Toilettes']
    dict_construction_year: list = ['Année de construction']
    dict_room: list = ['pièce', 'pièces']
    dict_bedroom: list = ['chambres', 'chambre']
    dict_cost_meter: list = ['€ / m²']
    dict_surface: list = ['m²']
    dic_exclusion_ref: list = ['Référence']
    dict_disabled: list = ['Cette annonce est désactivée']

    def __init__(self,response:Response, root_url:str):
        self.response = response
        self.root_url = root_url
        logger.error("RESPONSE: %s",self.response)

    
    def get_next_page(self):
        next_url: str = None
        try:
            next_url = self.response.xpath(self.xpath_next_url).get()
            next_url = self.root_url + next_url
        except Exception as e:
            logger.error("ERROR NEXT PAGE: %s",e)
            next_url = None
        
        return next_url
    
    def get_offers(self):
        offers:list = None
        try:
            offers = self.response.xpath(self.xpath_offers).extract()
            offers = [self.root_url +'{0}'.format(offer) for offer in offers]
        except Exception as e:
            offers = None
        
        
        logger.info("OFFERS: %s",offers)
        return offers

    def check_captcha(self):
        captcha = -1
        try:
            captcha = self.response.xpath(self.xpath_captcha).get()
            captcha = captcha.find("geo.captcha-delivery.com")
            
        
        except Exception as e:
            logger.error("ERROR CAPTCHA: %s",e)
        
        logger.info("CAPTCHA Value xpath: %s",captcha)
        logger.info("CAPTCHA type object: %s",type(captcha))
        if captcha != -1 and captcha != None:
            return True
        else:
            return False
    
    def extract_xpath(self,query):
        #TODO fix when use with self
        extract = self.response.xpath(query).getall()
        return extract

    def extract_multi_xpath(self,query):
        array:list = None
        for q in query:
            array.extend(self.extract_xpath(q))
        return array

    def parse_surface(self):
        surface: float = None
        surface = search_number_array(self.extract_xpath(self.xpath_surface))
        return surface

    def parse_announce_reference(self):
        ar:str = None
        try:
            ar = array_to_str(exclude_from_list(self.extract_xpath(self.xpath_ref), self.dic_exclusion_ref))
        except Exception as e:
            ar = ""
        return ar

    def parse_room(self):
        room:int = None
        room = search_number_array(self.extract_xpath(self.xpath_room))
        return room
        

    def parse_description(self):
        description: str = None
        description = array_to_str(self.extract_xpath(self.xpath_description))
        return description

    def parse_cost(self):
        cost: float = None
        logger.info("COST XPATH EXTRACT: %s",self.response.xpath(self.xpath_cost).getall())
        cost = search_number_array(self.response.xpath(self.xpath_cost).getall())
        logger.info("COST Value: %s",cost)
        return cost

    def parse_cost_per_meter(self):
        cpm:float = None
        try:
            cpm = round(self.parse_cost()/self.parse_surface())
        except Exception as e:
            cpm = 0
        
        return cpm


    def get_disabled(self):
        disabled_text: str = ""
        disabled: bool = False

        try:
            disabled_text = array_to_str(self.extract_xpath(self.xpath_disabled))
            logger.info("disabled_text: %s", disabled_text)

            if disabled_text in self.dict_disabled:
                disabled = True
            else:
                disabled = False

        except Exception as e:
            logger.info("ERROR: %s",e)
            disabled = False

        return disabled

class ParserSLG(IParser):
    response: Response = None
    root_url: str = None

    xpath_next_url:str = "//li[contains(@class,'next')]/a/@href"
    xpath_offers:str = '//a[@name="classified-link"]/@href'
    xpath_captcha:str = "/html/body/iframe/@src"

    #xpath_surface:str = '//*[@data-qa-id="criteria_item_square"]//text()'
    #xpath_room: str = '//*[@data-qa-id="criteria_item_rooms"]//text()'
    #xpath_ref: str = '//*[@data-qa-id="criteria_item_custom_ref"]//text()'
    xpath_description: str = '//*[@id="showcase-description"]/div[1]/div/div/div[1]/p/text()'
    xpath_cost: str = '//*[@id="root"]/div/main/div[2]/div/div[1]/div[1]/div[3]/div[1]/div[2]/div/div/span/span/text()'
    xpath_sumup_div:str ='//*[@id="root"]/div/main/div[2]/div/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/*/text()'

    dict_bathroom: list = ['Salle de bain', 'Salles de bain', 'Salle d\'eau']
    dict_toilette: list = ['Toilette', 'Toilettes']
    dict_construction_year: list = ['Année de construction']
    dict_room: list = ['pièce', 'pièces']
    dict_bedroom: list = ['chambres', 'chambre']
    dict_cost_meter: list = ['€ / m²']
    dict_surface: list = ['m²']
    dic_exclusion_ref: list = ['Référence']

    def __init__(self,response:Response, root_url:str):
        self.response = response
        self.root_url = root_url
    
    def get_next_page(self):
        next_url: str = None
        try:
            next_url = self.response.xpath(self.xpath_next_url).get()
            next_url = self.root_url + next_url
        except Exception as e:
            logger.info("ERROR: %s",e)
            next_url = None
        
        return next_url
    
    def get_offers(self):
        offers:list = None
        try:
            offers = self.response.xpath(self.xpath_offers).extract()
            #offers = [self.root_url +'{0}'.format(offer) for offer in offers]
        except Exception as e:
            offers = None
        
        
        logger.info("OFFERS: %s",offers)
        return offers

    def check_captcha(self):
        captcha = -1
        try:
            captcha = self.response.xpath(self.xpath_captcha).get()
            captcha = captcha.find("geo.captcha-delivery.com")
            
        
        except Exception as e:
            logger.info("ERROR: %s",e)
        
        logger.info("CAPTCHA Value xpath: %s",captcha)
        logger.info("CAPTCHA type object: %s",type(captcha))
        if captcha != -1 and captcha != None:
            return True
        else:
            return False
    
    def extract_xpath(self,query):
        return self.response.xpath(query).extract()

    def extract_multi_xpath(self,query):
        array:list = None
        for q in query:
            array.extend(self.extract_xpath(q))
        return array

    def parse_surface(self):
        surface: float = None
        surface = search_number_array_by_query(self.get_sumup_div(),self.dict_surface,self.dict_cost_meter)
        return surface

    def parse_announce_reference(self):
        ar:str = None
        try:
            ar = array_to_str(exclude_from_list(self.extract_xpath(self.xpath_ref), self.dic_exclusion_ref))
        except Exception as e:
            ar = ""
        return ar

    def parse_room(self):
        room:int = None
        room = search_number_array_by_query(self.get_sumup_div(), self.dict_room)
        return room
        

    def parse_description(self):
        description: str = None
        description = array_to_str(self.extract_xpath(self.xpath_description))
        return description

    def parse_cost(self):
        cost: float = None
        cost = search_number_array(self.extract_xpath(self.xpath_cost))
        return cost

    def parse_cost_per_meter(self):
        cpm:float = None
        try:
            cpm = round(self.parse_cost()/self.parse_surface())
        except Exception as e:
            cpm = 0
        
        return cpm
    
    def get_sumup_div(self):
        return self.extract_xpath(self.xpath_sumup_div)
    
    def get_disabled(self):
        return False