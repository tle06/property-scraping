# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals

# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter
from scrapy.http import HtmlResponse
from scrapy.utils.python import to_bytes
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import ErrorInResponseException
from scrapy import signals
from scrapy.utils.project import get_project_settings
import logging

# Sources: https://stackoverflow.com/questions/31174330/passing-selenium-response-url-to-scrapy/31186730#31186730

logger = logging.getLogger('SLG')

class SeleniumDownloaderMiddleware(object):
    user_agent = get_project_settings().get("USER_AGENT") if get_project_settings().get("USER_AGENT") else ""
    selenium_executable_path = get_project_settings().get("CHROME_EXE_PATH")
    logger.info('CHROME DRIVER PATH:%s',selenium_executable_path)
    selenium_remote_host = get_project_settings().get("SELENIUM_REMOTE_HOST")
    logger.info('SELENIUM_REMOTE_HOST:%s',selenium_remote_host)
    logger.info('CHROME_HEADLESS:%s',get_project_settings().get("CHROME_HEADLESS"))
    chrome_headless = False if get_project_settings().get("CHROME_HEADLESS") == "0" else True
    logger.info('CHROME_HEADLESS:%s',chrome_headless)
    selenium_directory = get_project_settings().get("SELENIUM_DIRECTORY")
    logger.info('SELENIUM_DIRECTORY:%s',selenium_directory)

    @classmethod
    def from_crawler(cls, crawler):
        middleware = cls()
        crawler.signals.connect(middleware.spider_opened, signals.spider_opened)
        crawler.signals.connect(middleware.spider_closed, signals.spider_closed)
        return middleware

    def process_request(self, request, spider):
        request.meta['driver'] = self.driver  # to access driver from response
        self.driver.get(request.url)
        body = to_bytes(self.driver.page_source)  # body must be of type bytes 
        return HtmlResponse(self.driver.current_url, body=body, encoding='utf-8', request=request)

    def spider_opened(self, spider):
        options = Options()
        options.add_argument(f'user-agent={self.user_agent}')
        options.add_argument("--disable-blink-features=AutomationControlled")
        options.add_argument('--window-size=1420,1080')
        #options.add_argument("--disable-xss-auditor")
        #options.add_argument("--disable-web-security")
        #options.add_argument("--allow-running-insecure-content")
        options.add_argument("--no-sandbox")
        #options.add_argument("--disable-setuid-sandbox")
        #options.add_argument("--disable-webgl")
        #options.add_argument("--disable-popup-blocking")
        options.add_argument("disable-gpu")
        options.add_argument("log-level=3")
        #options.add_argument('lang=en_US.UTF-8')
        options.add_argument("--disable-infobars")
        options.add_argument("--enable-file-cookies")
        if self.selenium_directory:
            options.add_argument(f"user-data-dir={self.selenium_directory}")
        else:
            options.add_argument(f"user-data-dir=/tmp/selenium")

        if self.chrome_headless:
            logger.info('USE HEADLESS')
            options.add_argument("--headless")
        # options.add_argument("--incognito")



        #d = DesiredCapabilities.CHROME

        # self.headers={"User-Agent":self.user_agent,
        #             "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        #             "Connection": "keep - alive",
        #             "Accept-Encoding":"gzip, deflate, br",
        #             "authority": "www.seloger.com",
        #             "accept-language": "en-US,en;q=0.9",
        #             "cache-control": "no-cache",
        #             "dnt": "1",
        #             }

        #self.driver = webdriver.Chrome(self.selenium_executable_path)
        #self.driver = webdriver.Chrome(chrome_options=options)

        if self.selenium_remote_host:
            logger.info('USE REMOTE SELENIUM')
            self.driver = webdriver.Remote(self.selenium_remote_host, options=options)
        else:
            logger.info('USE LOCAL SELENIUM')
            self.driver = webdriver.Chrome(chrome_options=options, executable_path=self.selenium_executable_path)
            self.driver.maximize_window() 
            self.driver.implicitly_wait(10)
        
        #self.driver.delete_all_cookies()

    def spider_closed(self, spider):
        self.driver.close()