from sqlalchemy import create_engine, Column, Table, ForeignKey, MetaData, event
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Integer, String, Date, DateTime, Float, Boolean, Text, VARCHAR)
from scrapy.utils.project import get_project_settings
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.mysql import insert
from sqlalchemy.sql import text
import logging

Base = declarative_base()
logger = logging.getLogger('SLG')

def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(get_project_settings().get("CONNECTION_STRING"))


def create_table(engine):
    Base.metadata.create_all(engine)
    insert_init_values(engine)

def insert_init_values(engine):
    engine = db_connect()
    session = sessionmaker(bind=engine)()
    data = []

    if not session.query(ScrapeSources).filter(ScrapeSources.id.in_([1,2])).all():

        source1 = ScrapeSources(id=1,name='seloger.com')
        source2 = ScrapeSources(id=2,name='leboncoin.fr')
        data.append(source1)
        data.append(source2)

    if not session.query(ScrapeTypes).filter(ScrapeTypes.id.in_([1,2])).all():
        type1 = ScrapeTypes(id=1,name='buying')
        type2 = ScrapeTypes(id=2,name='renting')
        data.append(type1)
        data.append(type2)

    try:
        session.add_all(data)
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


class ScrapeProperties(Base):

    __tablename__ = 'scrape_properties'

    id = Column(Integer, autoincrement=True, unique=True, primary_key=True, nullable=False)
    type_id =  Column(Integer)
    source_id = Column(Integer, ForeignKey("scrape_sources.id", name="fk_scrape_properties_map_scrape_source_id"), index=True, nullable=False)
    created_date = Column(DateTime)
    url = Column(Text)
    surface = Column(Integer)
    cost_per_meter = Column(Float)
    cost = Column(Float)
    address = Column(Text)
    city = Column(Text)
    country = Column(VARCHAR(45))
    room = Column(Integer)
    bedroom = Column(Integer)
    bathroom = Column(Integer)
    toilette = Column(Integer)
    energy_indice_number = Column(Float)
    greenhouse_gas_number = Column(Float)
    construction_year = Column(Integer)
    agency_name = Column(Text)
    announce_reference = Column(Text)
    description = Column(Text)
    disabled = Column(Boolean, default=False,nullable=False, server_default="false")

    # def __repr__(self):
    #     return self.__str__()

    # def __str__(self):
    #     return "<ScrapeProperty(%(id)s)>" % self.__dict__

class ScrapeSources(Base):

    __tablename__ = 'scrape_sources'

    id = Column(Integer, autoincrement=True, primary_key=True, nullable=False)
    name = Column(Text, nullable=False)

    # def __repr__(self):
    #     return self.__str__()

    # def __str__(self):
    #     return "<ScrapeSource(%(id)s)>" % self.__dict__

class ScrapeTypes(Base):

    __tablename__ = 'scrape_types'

    id = Column(Integer, autoincrement=True, primary_key=True, nullable=False)
    name = Column(VARCHAR(45), nullable=False)

    # def __repr__(self):
    #     return self.__str__()

    # def __str__(self):
    #     return "<ScrapeType(%(id)s)>" % self.__dict__
