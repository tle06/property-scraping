# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from sqlalchemy.orm import sessionmaker
from properties.model import db_connect, create_table, ScrapeProperties
import logging
from properties.items import ScrapePropertyItem

logger = logging.getLogger('PIPELINE')

class SQLImportPipeline(object):

    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates deals table.
        """
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        """Save deals in the database.

        This method is called for every item pipeline component.
        """
        logger.info("PROCESS ITEM")
        if isinstance(item, ScrapePropertyItem):
            logger.info("PROCESS ITEM = CREATE ScrapePropertyItem")
            return self.store_property(item, spider)
        else:
            logger.info("PROCESS ITEM = FAILURE")
            return item

        

    def store_property(self,item, spider):
        session = self.Session()
        logger.info("URL to look for: %s", item['url'])
        data = session.query(ScrapeProperties).filter_by(url=item['url']).first()
        logger.info("DATA ID found: %s", data.id)

        if not data:
            logger.info("DATA empty, create new object")
            data = ScrapeProperties()
        
        data.type_id = item["type_id"]
        data.source_id = item["source_id"]
        data.created_date =  item["created_date"]
        data.url =  item["url"]
        data.surface =  item["surface"]
        data.cost_per_meter =  item["cost_per_meter"]
        data.cost =  item["cost"]
        data.address =  item["address"]
        data.city =  item["city"]
        data.country =  item["country"]
        data.room =  item["room"]
        data.bedroom =  item["bedroom"]
        data.bathroom =  item["bathroom"]
        data.toilette =  item["toilette"]
        data.energy_indice_number =  item["energy_indice_number"]
        data.greenhouse_gas_number =  item["greenhouse_gas_number"]
        data.construction_year =  item["construction_year"]
        data.agency_name =  item["agency_name"]
        data.announce_reference =  item["announce_reference"]
        data.description =  item["description"]
        
        if True in item['disabled']:
            data.disabled = True
        else:
            data.disabled = False
        
        logger.info("Disabled value: %s",data.disabled)  

        try:
            session.add(data)
            session.commit()
            logger.info("DATA LOAD in DB")
            logger.info("DATA ID: %s", data.id)
        except:
            session.rollback()
            logger.info("DATAROLLBACK")
            raise
        finally:
            session.close()

        return item