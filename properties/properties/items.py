# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapePropertyItem(scrapy.Item):
    source_id = scrapy.Field()
    type_id=scrapy.Field()
    created_date=scrapy.Field()
    url=scrapy.Field()
    surface=scrapy.Field()
    cost_per_meter=scrapy.Field()
    cost=scrapy.Field()
    address=scrapy.Field()
    city=scrapy.Field()
    country=scrapy.Field()
    room=scrapy.Field()
    bedroom=scrapy.Field()
    bathroom=scrapy.Field()
    toilette=scrapy.Field()
    energy_indice_number=scrapy.Field()
    greenhouse_gas_number=scrapy.Field()
    construction_year=scrapy.Field()
    agency_name=scrapy.Field()
    announce_reference=scrapy.Field()
    description=scrapy.Field()
    disabled=scrapy.Field()