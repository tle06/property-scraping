
from scrapy.utils.project import get_project_settings
import requests

def slack_send_message(message:str):
    url:str = get_project_settings().get('SLACK_WEBHOOK')
    headers:dict = {'Content-Type': 'application/json'}
    response = requests.post(url, json={"text": message},headers=headers)
    return response